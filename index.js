const pooledDownload = async (connect, save, downloadList, maxConcurrency) => {
  let connection;

  try {
    connection = await connect();
  }
  catch (e) {
    return Promise.reject('connection error');
  }

  const pool = new Pool(maxConcurrency);
  const { download, close } = connection;

  // Make all download requests concurrently.
  const concurrentDownloads = downloadList.map(p => pool.run(() => download(p)));

  try {
    // Wait for all downloads to complete
    return Promise.all(concurrentDownloads);
  }
  finally {
    // Cleanup connection
    close();
  }
}

/**
 * Pool uses a simple locking primitive to queue
 * downloads up to the maximum concurrency.
 */
class Pool {
  constructor(maxConcurrency) {
    this.currentSize = 0;
    this.delegateQueue = [];
    this.lockQueue = [];
    this.maxConcurrency = maxConcurrency;
  }

  lock() {
    let release;

    // Create a dummy promise to force caller to wait.
    const wait = new Promise((_resolve, _reject) => {
      release = _resolve;
    });

    // Store a reference to the Promise resolver.
    this.lockQueue.push({ release });

    return wait;
  }

  release() {
    const wait = this.lockQueue.shift();

    // Resolve next Promise in the queue and allow
    // original caller to continue execution.
    wait && wait.release();
  }

  async run(delegate) {
    const currentSize = this.currentSize;
    const maxConcurrency = this.maxConcurrency;

    // Check if the Pool worker can execute any more delegates
    // concurrently. Execution will be paused using the lock
    // mechanism.
    if (currentSize >= maxConcurrency) {
      await this.lock();
      console.log('lock released');
    }

    // Mutate the worker count on the instance.
    this.currentSize++;

    // Execute the delegate "asynchronously" by wrapping it in
    // a Promise -- but return that Promise immediately to prevent
    // blocking [Pool].run()
    return new Promise(async (resolve, reject) => {
      await delegate();
      resolve();
      this.release();
    });
  }
}


// ----------------------------------------------------------------------------
// Mock call to pooledDownload
// ----------------------------------------------------------------------------
const downloads = [1, 2, 3, 4, 5, 6, 7, 8, 9, , 10, 11, 12, 13, 14, 15, 16];
const maxConcurrency = 3;

pooledDownload(
  function connect() {
    console.log('connected');
    return Promise.resolve({
      close: () => console.log('closed'),
      download: (p) => {
        console.log('downloading ', p);
        // Simulate slow download by delaying Promise resolution.
        return new Promise((resolve, reject) => setTimeout(() => resolve(), 3000));
      }
    })
  },
  function save() { return Promise.resolve() },
  downloads,
  maxConcurrency);
